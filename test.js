let axios = require('axios');
const HttpsProxyAgent = require("https-proxy-agent");

const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms));
// const httpsAgent = new HttpsProxyAgent({host: "20.6.0.172", port: "80"})
// axios = axios.create({httpsAgent});
const findPrice = 4300;
const urlForGetPlayers = `https://utas.mob.v2.fut.ea.com/ut/game/fc24/transfermarket?num=21&start=0&type=player&maskedDefId=226753&maxb=${findPrice}`;

const token = 'f43871af-6bd8-4d7f-a691-4c2451ae4317';

let i = 0;
const boughtArray = [];
const buyPrice = 3800;
let tradeId;
(async () => {
  while(true) {
    try {
      i++;
      const players = await axios.get(urlForGetPlayers, {
        // proxy: {
        //   host: "20.6.0.172",
        //   port: "80"
        // },
        headers: {
          'Host': '192.168.100.100',
          'Accept': '*/*',
          'Accept-Language': 'en-US,en;q=0.9,ru-UA;q=0.8,ru;q=0.7,uk-UA;q=0.6,uk;q=0.5,ru-RU;q=0.4',
          'Cache-Control': 'no-cache',
          'Connection': 'keep-alive',
          'Content-Type': 'application/json',
          'Origin': 'https://www.ea.com',
          'Referer': 'https://www.ea.com/',
          'Sec-Fetch-Dest': 'empty',
          'Sec-Fetch-Mode': 'cors',
          'Sec-Fetch-Site': 'same-site',
          'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Mobile Safari/537.36',
          'X-UT-SID': token,
          'sec-ch-ua': '"Chromium";v="116", "Not)A;Brand";v="24", "Google Chrome";v="116"',
          'sec-ch-ua-mobile': '?1',
          'sec-ch-ua-platform': 'Android',
        }
      });
      console.log(players.data.auctionInfo.length)
      const toBuyItems = players.data.auctionInfo.filter(el => el.buyNowPrice <= buyPrice && !boughtArray.includes(el.tradeId));
      const toBuyItem = toBuyItems[toBuyItems.length - 1];

        tradeId = toBuyItem?.tradeId;
        if (toBuyItem) {
          console.log('-=-==--==-=-', toBuyItem.buyNowPrice)
          await axios.put(`https://utas.mob.v2.fut.ea.com/ut/game/fc24/trade/${tradeId}/bid`, {
            bid: toBuyItem.buyNowPrice,
          }, {
            headers: {
              'Accept': '*/*',
              'Accept-Language': 'en-US,en;q=0.9,ru-UA;q=0.8,ru;q=0.7,uk-UA;q=0.6,uk;q=0.5,ru-RU;q=0.4',
              'Cache-Control': 'no-cache',
              'Connection': 'keep-alive',
              'Content-Type': 'application/json',
              'Origin': 'https://www.ea.com',
              'Referer': 'https://www.ea.com/',
              'Sec-Fetch-Dest': 'empty',
              'Sec-Fetch-Mode': 'cors',
              'Sec-Fetch-Site': 'same-site',
              'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Mobile Safari/537.36',
              'X-UT-SID': token,
              'sec-ch-ua': '"Chromium";v="116", "Not)A;Brand";v="24", "Google Chrome";v="116"',
              'sec-ch-ua-mobile': '?1',
              'sec-ch-ua-platform': 'Android',
            }
          });
          boughtArray.push(tradeId);
          console.log('uhuhuuuuuu: ', toBuyItem.buyNowPrice)
          console.log(toBuyItem)
        }
      console.log('----')
      if (i=== 15) {
        await delay(3000);
        i=0;
      } else {
        await delay(2000);
      }
    } catch (e) {
      console.log(tradeId)
      boughtArray.push(tradeId);
      await delay(2000);
      console.log('Error', e.message)
    }
  }


})()
